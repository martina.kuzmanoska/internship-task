package mk.codeit.internshiptask.city.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.codeit.internshiptask.city.dto.CityDto;
import mk.codeit.internshiptask.city.entity.City;
import mk.codeit.internshiptask.city.repository.CityRepository;
import mk.codeit.internshiptask.country.entity.Country;
import mk.codeit.internshiptask.country.service.CountryService;

@Service
public class CityService {

	@Autowired
	public CityRepository cityRepository;

	@Autowired
	private CountryService countryService;

	public void createCity(CityDto cityDto) {
		Country country = this.countryService.getCountry(cityDto.getCountryId());

		if (country != null) {
			City city = new City(cityDto.getName(), cityDto.isCapital(), country);

			this.cityRepository.save(city);
		}
	}

	public void createCities(List<CityDto> cityDtos) {
		for (CityDto cityDto : cityDtos) {
			this.createCity(cityDto);
		}
	}

	public List<City> getCities() {
		return this.cityRepository.findAll();
	}

	public City getCity(long id) {
		Optional<City> optional = this.cityRepository.findById(id);
		City city = optional.orElse(null);

		return city;
	}

	public List<City> getCitiesByCountry(String countryName) {
		return this.cityRepository.findAllByCountryNameIgnoreCase(countryName);
	}

	public void updateCity(long id, CityDto cityDto) {
		City city = this.getCity(id);
		Country country = this.countryService.getCountry(cityDto.getCountryId());

		if (city != null && country != null) {
			city.setName(cityDto.getName());
			city.setCapital(false);
			city.setCountry(country);

			this.cityRepository.save(city);
		}
	}

	public void deleteCity(long id) {
		this.cityRepository.deleteById(id);
	}
}
