package mk.codeit.internshiptask.city.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.codeit.internshiptask.city.dto.CityDto;
import mk.codeit.internshiptask.city.entity.City;
import mk.codeit.internshiptask.city.service.CityService;
import mk.codeit.internshiptask.country.entity.Country;

@RestController
@RequestMapping("/city")
public class CityController {
	
	@Autowired
	private CityService cityService;
	
	@PostMapping
	public void createCity(@RequestBody CityDto cityDto) {
		this.cityService.createCity(cityDto);
	}
	
	@PostMapping("/batch")
	public void createCities(@RequestBody List<CityDto> cityDtos) {
		this.cityService.createCities(cityDtos);
	}
	
	@GetMapping
	public List<City> getCities() {
		return this.cityService.getCities();
	}
	
	@GetMapping("/{id}")
	public City getCity(@PathVariable("id") long id) {
		return this.cityService.getCity(id);
	}
	
	@GetMapping("/country/{countryName}")
	public List<City> getCitiesByCountry(@PathVariable("country") String countryName) {
		return this.cityService.getCitiesByCountry(countryName);
	}
	
	@PutMapping("/{id}")
	public void updateCity(@PathVariable("id") long id, @RequestBody CityDto cityDto) {
		this.cityService.updateCity(id, cityDto);
	}
	
	@DeleteMapping("/{id}")
	public void deleteCity(long id) {
		this.cityService.deleteCity(id);
	}
	
}
