package mk.codeit.internshiptask.city.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mk.codeit.internshiptask.city.entity.City;

public interface CityRepository extends CrudRepository<City, Long> {

	public List<City> findAll();
	
	public List<City> findAllByCountryNameIgnoreCase(String countryName);
	
	//@Query("SELECT c FROM City c WHERE c.capital = TRUE")
	//public List<City> findAllCapitalCities();
	// find all capital cities
}

/*
interface Playable {
	public void play();
}

interface Stoppable {
	public void stop();
} 

class Song implements Playable, Stoppable {
	
	public void play() {
		
	}
	
	public void stop() {
		
	}
}
*/
