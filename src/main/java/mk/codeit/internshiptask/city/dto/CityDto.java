package mk.codeit.internshiptask.city.dto;

public class CityDto {
	
	private String name;
	
	private boolean capital;
	
	private long countryId;

	public CityDto(String name, boolean capital, long countryId) {
		this.name = name;
		this.capital = capital;
		this.countryId = countryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCapital() {
		return capital;
	}

	public void setCapital(boolean capital) {
		this.capital = capital;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	
}
