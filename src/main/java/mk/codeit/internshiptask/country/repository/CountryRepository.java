package mk.codeit.internshiptask.country.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mk.codeit.internshiptask.country.entity.Country;

public interface CountryRepository extends CrudRepository<Country, Long> {

	public List<Country> findAll();
}
