package mk.codeit.internshiptask.country.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.codeit.internshiptask.country.dto.CountryDto;
import mk.codeit.internshiptask.country.entity.Country;
import mk.codeit.internshiptask.country.repository.CountryRepository;

@Service
public class CountryService {

	@Autowired
	public CountryRepository countryRepository;

	public void createCountry(CountryDto countryDto) {
		Country country = new Country(countryDto.getName(), countryDto.getIso3());

		this.countryRepository.save(country);
	}

	public void createCountries(List<CountryDto> countryDtos) {
		for (CountryDto countryDto : countryDtos) {
			this.createCountry(countryDto);
		}
	}

	public List<Country> getCountries() {
		return this.countryRepository.findAll();
	}

	public Country getCountry(long id) {
		Optional<Country> optional = this.countryRepository.findById(id);
		Country country = optional.orElse(null);

		return country;
	}

	public void updateCountry(long id, CountryDto countryDto) {
		Country country = this.getCountry(id);

		if (country != null) {
			country.setName(countryDto.getName());
			country.setIso3(countryDto.getIso3());

			this.countryRepository.save(country);
		}
	}

	public void deleteCountry(long id) {
		this.countryRepository.deleteById(id);
	}

}
