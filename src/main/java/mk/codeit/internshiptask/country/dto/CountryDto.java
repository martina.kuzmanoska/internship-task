package mk.codeit.internshiptask.country.dto;

public class CountryDto {

	private String name;
	private String iso3;

	public CountryDto(String name, String iso3) {
		this.name = name;
		this.iso3 = iso3;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIso3() {
		return iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

}
