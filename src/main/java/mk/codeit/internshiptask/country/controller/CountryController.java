package mk.codeit.internshiptask.country.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.codeit.internshiptask.country.dto.CountryDto;
import mk.codeit.internshiptask.country.entity.Country;
import mk.codeit.internshiptask.country.service.CountryService;

@RestController
@RequestMapping("/country")
public class CountryController {

	@Autowired
	private CountryService countryService;

	@PostMapping
	public void createCountry(@RequestBody CountryDto countryDto) {
		this.countryService.createCountry(countryDto);
	}

	@PostMapping("/batch")
	public void createCountries(@RequestBody List<CountryDto> countryDtos) {
		this.countryService.createCountries(countryDtos);
	}

	@GetMapping
	public List<Country> getCountries() {
		return this.countryService.getCountries();
	}

	@GetMapping("/{id}")
	public Country getCountry(@PathVariable("id") long id) {
		return this.countryService.getCountry(id);
	}

	@PutMapping("/{id}")
	public void updateCountry(@PathVariable("id") long id, @RequestBody CountryDto countryDto) {
		this.countryService.updateCountry(id, countryDto);
	}

	@DeleteMapping("/{id}")
	public void deleteCountry(@PathVariable("id") long id) {
		this.countryService.deleteCountry(id);
	}

}
