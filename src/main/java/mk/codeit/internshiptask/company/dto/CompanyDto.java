package mk.codeit.internshiptask.company.dto;

public class CompanyDto {

	private String companyName;

	private int foundationYear;
	
	private long addressId;

	public CompanyDto(String companyName, int foundationYear, long addressId) {
		this.companyName = companyName;
		this.foundationYear = foundationYear;
		this.addressId = addressId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getFoundationYear() {
		return foundationYear;
	}

	public void setFoundationYear(int foundationYear) {
		this.foundationYear = foundationYear;
	}
	
	public long getAddressId() {
		return addressId;
	}
	
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

}
