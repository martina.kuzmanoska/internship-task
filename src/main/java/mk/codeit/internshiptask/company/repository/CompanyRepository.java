package mk.codeit.internshiptask.company.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mk.codeit.internshiptask.company.entity.Company;

public interface CompanyRepository extends CrudRepository<Company, Long> {

	public List<Company> findAll();
}
