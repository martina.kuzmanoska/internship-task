package mk.codeit.internshiptask.company.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.codeit.internshiptask.address.entity.Address;
import mk.codeit.internshiptask.address.service.AddressService;
import mk.codeit.internshiptask.company.dto.CompanyDto;
import mk.codeit.internshiptask.company.entity.Company;
import mk.codeit.internshiptask.company.repository.CompanyRepository;

@Service
public class CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private AddressService addressService;

	public void createCompany(CompanyDto companyDto) {
		Address address = this.addressService.getAddress(companyDto.getAddressId());

		if (address != null) {
			Company company = new Company(companyDto.getCompanyName(), companyDto.getFoundationYear(), address);

			this.companyRepository.save(company);
		}
	}

	public void createCompanies(List<CompanyDto> companyDtos) {
		for (CompanyDto companyDto : companyDtos) {
			this.createCompany(companyDto);
		}
	}

	public List<Company> getCompanies() {
		return this.companyRepository.findAll();
	}

	public Company getCompany(long id) {
		Optional<Company> optional = this.companyRepository.findById(id);
		Company company = optional.orElse(null);

		return company;
	}

	public void updateCompany(long id, CompanyDto companyDto) {
		Company company = this.getCompany(id);
		Address address = this.addressService.getAddress(companyDto.getAddressId());

		if (company != null && address != null) {
			company.setCompanyName(companyDto.getCompanyName());
			company.setAddress(address);

			this.companyRepository.save(company);
		}
	}

	public void deleteCompany(long id) {
		this.companyRepository.deleteById(id);
	}

}
