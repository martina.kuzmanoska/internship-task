package mk.codeit.internshiptask.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.codeit.internshiptask.company.dto.CompanyDto;
import mk.codeit.internshiptask.company.entity.Company;
import mk.codeit.internshiptask.company.service.CompanyService;

@RestController
@RequestMapping("/company")
public class CompanyController {

	@Autowired
	private CompanyService companyService;

	@PostMapping
	public void createCompany(@RequestBody CompanyDto companyDto) {
		this.companyService.createCompany(companyDto);
	}

	@PostMapping("/batch")
	public void createCompanies(@RequestBody List<CompanyDto> companyDtos) {
		this.companyService.createCompanies(companyDtos);
	}

	@GetMapping
	public List<Company> getCompanies() {
		return this.companyService.getCompanies();
	}

	@GetMapping("/{id}")
	public Company getCompany(@PathVariable("id") long id) {
		return this.companyService.getCompany(id);
	}

	@PutMapping("/{id}")
	public void updateCompany(@PathVariable("id") long id, @RequestBody CompanyDto companyDto) {
		this.companyService.updateCompany(id, companyDto);
	}

	@DeleteMapping("/{id}")
	public void deleteCompany(@PathVariable("id") long id) {
		this.companyService.deleteCompany(id);
	}

}
