package mk.codeit.internshiptask.address.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mk.codeit.internshiptask.address.entity.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {

	public List<Address> findAll();

	public List<Address> findAllByCityNameIgnoreCase(String city);
}
