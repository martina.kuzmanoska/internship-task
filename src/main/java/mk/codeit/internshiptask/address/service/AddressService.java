package mk.codeit.internshiptask.address.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.codeit.internshiptask.address.dto.AddressDto;
import mk.codeit.internshiptask.address.entity.Address;
import mk.codeit.internshiptask.address.repository.AddressRepository;
import mk.codeit.internshiptask.city.entity.City;
import mk.codeit.internshiptask.city.service.CityService;

@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private CityService cityService;

	public void createAddress(AddressDto addressDto) {
		City city = this.cityService.getCity(addressDto.getCityId());

		if (city != null) {
			Address address = new Address(city, addressDto.getStreet(), addressDto.getNumber());

			this.addressRepository.save(address);
		}
	}

	public void createAddresses(List<AddressDto> addressDtos) {
		for (AddressDto addressDto : addressDtos) {
			this.createAddress(addressDto);
		}
	}

	public List<Address> getAddresses() {
		return this.addressRepository.findAll();
	}

	public Address getAddress(long id) {
		Optional<Address> optional = this.addressRepository.findById(id);
		Address address = optional.orElse(null);

		return address;
	}

	public void updateAddress(long id, AddressDto addressDto) {
		Address address = this.getAddress(id);
		City city = this.cityService.getCity(addressDto.getCityId());

		if (address != null && city != null) {
			address.setCity(city);
			address.setStreet(addressDto.getStreet());
			address.setNumber(addressDto.getNumber());

			this.addressRepository.save(address);
		}
	}

	public void deleteAddress(long id) {
		this.addressRepository.deleteById(id);
	}

	public List<Address> getAddressesByCity(String city) {
		return this.addressRepository.findAllByCityNameIgnoreCase(city);
	}

}
