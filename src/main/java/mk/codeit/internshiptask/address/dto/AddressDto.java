package mk.codeit.internshiptask.address.dto;

public class AddressDto {

	private long cityId;

	private String street;

	private int number;

	public AddressDto(long cityId, String street, int number) {
		this.cityId = cityId;
		this.street = street;
		this.number = number;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
