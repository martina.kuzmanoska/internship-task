package mk.codeit.internshiptask.address.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.codeit.internshiptask.address.dto.AddressDto;
import mk.codeit.internshiptask.address.entity.Address;
import mk.codeit.internshiptask.address.service.AddressService;

@RestController
@RequestMapping("/address")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@PostMapping
	public void createAddress(@RequestBody AddressDto addressDto) {
		this.addressService.createAddress(addressDto);
	}

	@PostMapping("/batch")
	public void createAddresses(@RequestBody List<AddressDto> addressDtos) {
		this.addressService.createAddresses(addressDtos);
	}

	@GetMapping
	public List<Address> getAddresses() {
		return this.addressService.getAddresses();
	}

	/*
	 * @GetMapping("/{id}") public Address getAddress(@PathVariable("id") long id) {
	 * return this.addressService.getAddress(id); }
	 */
	@PutMapping("/{id}")
	public void updateAddress(@PathVariable("id") long id, @RequestBody AddressDto addressDto) {
		this.addressService.updateAddress(id, addressDto);
	}

	@DeleteMapping("/{id}")
	public void deleteAddress(@PathVariable("id") long id) {
		this.addressService.deleteAddress(id);
	}

	@GetMapping("/{city}")
	public List<Address> getAddressesByCity(@PathVariable("city") String city) {
		return this.addressService.getAddressesByCity(city);
	}

}
