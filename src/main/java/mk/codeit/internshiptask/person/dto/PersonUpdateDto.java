package mk.codeit.internshiptask.person.dto;

public class PersonUpdateDto {

	private int age;

	private String profession;

	private long companyId;

	public PersonUpdateDto(int age, String profession, long companyId) {
		this.age = age;
		this.profession = profession;
		this.companyId = companyId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

}
