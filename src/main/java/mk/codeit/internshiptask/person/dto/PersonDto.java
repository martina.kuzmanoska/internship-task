package mk.codeit.internshiptask.person.dto;

public class PersonDto {

	private String firstName;

	private String lastName;

	private int age;

	private String profession;

	private long companyId;

	public PersonDto(String firstName, String lastName, int age, String profession, long companyId) {
	
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.profession = profession;
		this.companyId = companyId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

}
