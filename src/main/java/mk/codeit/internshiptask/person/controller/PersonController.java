package mk.codeit.internshiptask.person.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mk.codeit.internshiptask.person.dto.PersonDto;
import mk.codeit.internshiptask.person.dto.PersonUpdateDto;
import mk.codeit.internshiptask.person.entity.Person;
import mk.codeit.internshiptask.person.service.PersonService;

@RestController
@RequestMapping("/person")
public class PersonController {

	@Autowired
	private PersonService personService;

	@PostMapping
	public void createPerson(@RequestBody PersonDto personDto) {
		this.personService.createPerson(personDto);
	}

	@PostMapping("/batch")
	public void createPersons(@RequestBody List<PersonDto> personDtos) {
		this.personService.createPersons(personDtos);
	}

	@GetMapping
	public List<Person> getPersons() {
		return this.personService.getPersons();
	}

	@GetMapping("/{id}")
	public Person getPerson(@PathVariable("id") long id) {
		return this.personService.getPerson(id);
	}

	@PutMapping("/{id}")
	public void updatePerson(@PathVariable("id") long id, @RequestBody PersonUpdateDto personUpdateDto) {
		this.personService.updatePerson(id, personUpdateDto);
	}

	@DeleteMapping("/{id}")
	public void deletePerson(@PathVariable("id") long id) {
		this.personService.deletePerson(id);
	}
}
