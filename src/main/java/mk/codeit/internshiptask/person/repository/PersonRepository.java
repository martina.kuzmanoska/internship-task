package mk.codeit.internshiptask.person.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import mk.codeit.internshiptask.person.entity.Person;

public interface PersonRepository extends CrudRepository<Person, Long> {

	public List<Person> findAll();
}
