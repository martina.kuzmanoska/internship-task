package mk.codeit.internshiptask.person.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.codeit.internshiptask.company.entity.Company;
import mk.codeit.internshiptask.company.service.CompanyService;
import mk.codeit.internshiptask.person.dto.PersonDto;
import mk.codeit.internshiptask.person.dto.PersonUpdateDto;
import mk.codeit.internshiptask.person.entity.Person;
import mk.codeit.internshiptask.person.repository.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private CompanyService companyService;

	public void createPerson(PersonDto personDto) {
		Company company = this.companyService.getCompany(personDto.getCompanyId());

		if (company != null) {
			Person person = new Person(personDto.getFirstName(), personDto.getLastName(), personDto.getAge(),
				personDto.getProfession(), company);

			this.personRepository.save(person);
		}
	}

	public void createPersons(List<PersonDto> personDtos) {
		for (PersonDto personDto : personDtos) {
			this.createPerson(personDto);
		}
	}

	public List<Person> getPersons() {
		return this.personRepository.findAll();
	}

	public Person getPerson(long id) {
		Optional<Person> optional = this.personRepository.findById(id);
		Person person = optional.orElse(null);

		return person;
	}

	public void updatePerson(long id, PersonUpdateDto personUpdateDto) {
		Person person = this.getPerson(id);
		Company company = this.companyService.getCompany(personUpdateDto.getCompanyId());

		if (person != null) { // && company !=null) {
			person.setAge(personUpdateDto.getAge());
			person.setProfession(personUpdateDto.getProfession());
			person.setCompany(company); // kako??

			this.personRepository.save(person);
		}
	}

	public void deletePerson(long id) {
		this.personRepository.deleteById(id);
	}
}
