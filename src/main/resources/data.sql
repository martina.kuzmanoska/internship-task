INSERT INTO Country (id, name, iso3) VALUES (1, 'United States of America', 'USD');
INSERT INTO Country (id, name, iso3) VALUES (2, 'Macedonia', 'MKD');
INSERT INTO Country (id, name, iso3) VALUES (3, 'Italy', 'ITA');
INSERT INTO Country (id, name, iso3) VALUES (4, 'Spain', 'ESP');
INSERT INTO Country (id, name, iso3) VALUES (5, 'United Kingdom', 'GBR');


INSERT INTO City (id, name, capital, country_id) VALUES (1, 'Skopje', true, (SELECT id FROM Country WHERE iso3 = 'MKD'));
INSERT INTO City (id, name, capital, country_id) VALUES (2, 'Prilep', false, (SELECT id FROM Country WHERE iso3 = 'MKD'));
INSERT INTO City (id, name, capital, country_id) VALUES (3, 'Ohrid', false, (SELECT id FROM Country WHERE iso3 = 'MKD'));
INSERT INTO City (id, name, capital, country_id) VALUES (4, 'Bitola', false, (SELECT id FROM Country WHERE iso3 = 'MKD'));
INSERT INTO City (id, name, capital, country_id) VALUES (5, 'Valencia', false, (SELECT id FROM Country where iso3 = 'ESP'));


INSERT INTO Address (id, city_id, street, number) VALUES (1, (SELECT id FROM City WHERE name = 'Skopje'), 'Jane Sandanski', 119);
INSERT INTO Address (id, city_id, street, number) VALUES (2, (SELECT id FROM City WHERE name = 'Skopje'), 'Jane Sandanski', 37);
INSERT INTO Address (id, city_id, street, number) VALUES (3, (SELECT id FROM City WHERE name = 'Prilep'), 'Ljube Grueski', 91);
INSERT INTO Address (id, city_id, street, number) VALUES (4, (SELECT id FROM City WHERE name = 'Skopje'), '1161', 20);
INSERT INTO Address (id, city_id, street, number) VALUES (5, (SELECT id FROM City WHERE name = 'Valencia'), 'Calle de Pavia', 35);


INSERT INTO Company (id, company_name, foundation_year, address_id) VALUES (1, 'CodeIt', 2012, (SELECT id FROM Address WHERE number = 119));
INSERT INTO Company (id, company_name, foundation_year, address_id) VALUES (2, 'CuciBuci Inc', 2017, (SELECT id FROM Address WHERE number = 91));
INSERT INTO Company (id, company_name, foundation_year, address_id) VALUES (3, 'Baba Inc', 2021, (SELECT id FROM Address WHERE number = 20));
INSERT INTO Company (id, company_name, foundation_year, address_id) VALUES (4, 'Descarada', 2020, (SELECT id FROM Address WHERE number = 35));


INSERT INTO Person (id, age, first_name, last_name, profession, company_id) VALUES (1, 32, 'Martina', 'Kuzmanoska', 'intern', (SELECT id FROM Company WHERE company_name = 'CodeIt'));
INSERT INTO Person (id, age, first_name, last_name, profession, company_id) VALUES (2, 30, 'Ivan', 'Georgiev', 'developer', (SELECT id FROM Company WHERE company_name = 'CodeIt'));
INSERT INTO Person (id, age, first_name, last_name, profession, company_id) VALUES (3, 3, 'Matea', 'Kuzmanoska', 'dragon guardian', (SELECT id FROM Company WHERE company_name = 'CuciBuci Inc'));
INSERT INTO Person (id, age, first_name, last_name, profession, company_id) VALUES (4, 30, 'Baba', 'Bapchorka', 'critic', (SELECT id FROM Company WHERE company_name = 'Baba Inc'));
INSERT INTO Person (id, age, first_name, last_name, profession, company_id) VALUES (5, 28, 'Abuela', 'Lopez', 'tester', (SELECT id FROM Company WHERE company_name = 'Descarada'));